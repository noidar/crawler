const getData = document.querySelector('.get-data');
const sendMessage = document.querySelector('.sendMessage');
const mainTag = document.querySelector('main');
const performCrowl = document.querySelector('.perform-crowl');

const mainUrl = 'https://crawler-r.herokuapp.com/';
const authUrl = 'authentication/';
const messagesUrl = 'messages';
const ABOUT = 'about';
const logInForm = document.querySelector('.log-in-form');

const listMessages = document.querySelector('.list-messages');

// [input] log In
const inputEmail = document.getElementById('inputEmail');
const inputPassword = document.getElementById('inputPassword');
const submitCredentialsButton = document.querySelector('.submitCredentialsButton');

// [input] send messages
const senderName = document.getElementById('senderName');
const senderMessage = document.getElementById('senderMessage');

// [button] get about data
const getAbout = document.querySelector('.get-about');
const about = document.querySelector('.about');

const access_token = localStorage.getItem('accessToken');

if (!access_token) {
  logInForm.classList.toggle('visible');
}

function requestHandeler(url, callBack, params, method = 'GET', options) {
  const xhr = new XMLHttpRequest();

  if (params && !access_token) {
    xhr.open(method, url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = callBack;
    xhr.send(JSON.stringify(params));
    console.log(params, '[SEND PARAMS]');

  }

  if (access_token && !options) {
    xhr.open(method, url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
    xhr.onload = callBack;
    xhr.send(JSON.stringify(params));
    // console.log(params, '[Post message]');
  }

  if (options) {
    xhr.open(method, url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
    xhr.onload = callBack;
    xhr.send();
  }

}

function reqListener() {
  // console.log(this.responseText);
}

function makeRequest() {
  const url = 'https://crawler-r.herokuapp.com/api/v1/data';
  requestHandeler(url, reqListener)
}

// log in
function handleResponseAuth() {
  const responseStatus = this.status;

  const responseToken = this.responseText;

  if (responseStatus !== 400) {
    localStorage.setItem("accessToken", JSON.parse(responseToken).accessToken);

    removeFormLog();
  }

  if (responseStatus === 400) {
    throw new Error(responseStatus.responseText);
  }

}

function logInHandler() {
  const email = inputEmail.value;
  const password = inputPassword.value;

  const params = {
    strategy: "local",
    email,
    password,
  };

  requestHandeler(`${mainUrl + authUrl}`, handleResponseAuth, params, 'POST');
}

// ========= Succes LogIn ================
function removeFormLog() {
  mainTag.removeChild(logInForm);

}


// ------------------------------------------
function printMessages() {

  const responseData = JSON.parse(this.response);
  const ul = document.createElement('ul');

  responseData.data.map(message => {
    const li = document.createElement('li');

    li.innerHTML = `<li class="list-group-item"><small class="name">${message.name}</small>${message.text}</li>`;

    ul.appendChild(li);
  }
  );

  if (listMessages.childNodes) {
    listMessages.removeChild(listMessages.firstChild);
  }

  listMessages.appendChild(ul);
  console.log(responseData);
  console.log(ul);
}

function getCrawData() {
  requestHandeler(`${mainUrl}${messagesUrl}`, printMessages)
}

submitCredentialsButton.addEventListener('click', logInHandler);
getData.addEventListener('click', getCrawData);

// =========== Inserting a message ===========
function handleStatusSendMessage() {
  console.log(this.responseText, '[handleStatusSendMessage]');
}

function sendMessagesHandler() {
  const name = senderName.value;
  const text = senderMessage.value;

  const params = { name, text }

  requestHandeler(`${mainUrl + messagesUrl}`, handleStatusSendMessage, params, 'POST');
}

sendMessage.addEventListener('click', sendMessagesHandler, false);


// ------------- get about handle -----------------
function handleAboutResponse() {
  about.textContent =  this.response;
}

function getAboutInfo() {
  requestHandeler(`${mainUrl + ABOUT}`, handleAboutResponse);
}

getAbout.addEventListener('click', getAboutInfo, false);
