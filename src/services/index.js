const messages = require('./messages/messages.service.js');
const users = require('./users/users.service.js');
const about = require('./about/about.service.js');
const testsequalise = require('./testsequalise/testsequalise.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(messages);
  app.configure(users);
  app.configure(about);
  app.configure(testsequalise);
};
