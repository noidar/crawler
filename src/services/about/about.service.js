// Initializes the `about` service on path `/about`
// const createService = require('feathers-nedb');
const createService = require('./about.class');
const createModel = require('../../models/about.model');

const Service = require('./about.class')
const hooks = require('./about.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/about', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('about');

  service.hooks(hooks);
};
