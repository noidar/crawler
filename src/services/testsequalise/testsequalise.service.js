// Initializes the `testsequalise` service on path `/testsequalise`
const createService = require('feathers-sequelize');
const createModel = require('../../models/testsequalise.model');
const hooks = require('./testsequalise.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/testsequalise', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('testsequalise');

  service.hooks(hooks);
};
