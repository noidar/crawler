const assert = require('assert');
const app = require('../../src/app');

describe('\'testsequalise\' service', () => {
  it('registered the service', () => {
    const service = app.service('testsequalise');

    assert.ok(service, 'Registered the service');
  });
});
