const assert = require('assert');
const app = require('../../src/app');

describe('\'about\' service', () => {
  it('registered the service', () => {
    const service = app.service('about');

    assert.ok(service, 'Registered the service');
  });
});
